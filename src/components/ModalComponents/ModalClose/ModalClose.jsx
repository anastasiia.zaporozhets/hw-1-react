import "./ModalClose.scss"
import closeImg from "../../../assets/closeCross.png"

function ModalClose({onClick}) {
    return (
        <img className="modal-close" onClick={onClick} src={closeImg} alt="icon-close"/>
    )
}

export default ModalClose;