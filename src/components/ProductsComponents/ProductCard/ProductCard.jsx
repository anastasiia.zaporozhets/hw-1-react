import "../ProductCard/ProductCard.scss"
import PropTypes from "prop-types";
import Button from "../../Button/Button.jsx";

function ProductCard({item, onBuyClick, clickStar}) {

    const handleStarClick = (event) => {
        event.preventDefault();
        clickStar(item.id);
    };

    const isFavorite = localStorage.getItem('favorite') && JSON.parse(localStorage.getItem('favorite')).includes(item.id);

    return (
        <div className="product-list__card">
            <a href="#" onClick={handleStarClick} title="додати в обране">
                <svg className={`product-list__icon-star ${isFavorite ? 'product-list__icon-storage' : ''}`} width="30"
                     height="25" fill="none"
                     xmlns="http://www.w3.org/2000/svg">
                    <path
                        d="M19.467,23.316,12,17.828,4.533,23.316,7.4,14.453-.063,9H9.151L12,.122,14.849,9h9.213L16.6,14.453Z"
                        stroke="#807D7E" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round"/>
                </svg>
            </a>
            <img src={item.imageUrl} alt="product" className="product-list__card__img"/>
            <div className="product-list__card__wrapper-text">
                <p className="product-list__card__wrapper-text__name">{item.name}</p>
                <p className="product-list__card__wrapper-text__color-text">Колір: {item.color}</p>
            </div>
            <div className="product-list__card__price-wrapper">
                <p className="product-list__card__price-wrapper__price">Ціна {item.price} грн</p>
            </div>
            <Button type="button" className="btn btn__main btn__main__first" onClick={() => onBuyClick(item)}>
                У КОШИК
            </Button>
        </div>
    );
}

ProductCard.propTypes = {
    item: PropTypes.shape({
        id: PropTypes.number.isRequired,
        imageUrl: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        color: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
        article: PropTypes.string.isRequired,
    }).isRequired,
    onBuyClick: PropTypes.func.isRequired,
    clickStar: PropTypes.func.isRequired,
};

export default ProductCard