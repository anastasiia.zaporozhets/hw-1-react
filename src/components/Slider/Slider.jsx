import "../Slider/Slider.scss"
import SliderImage from "../../assets/Slider1.png"
function Slider() {
    return(
        <section className="section-slider">
            <img className="section-slider__img" src={SliderImage} alt=""/>
        </section>
    )
}

export default Slider